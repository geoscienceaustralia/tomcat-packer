#deregister previous script deregisters AMIs that have the same name as the packer output AMI, that are older. 
# This ensures only one master AMI is registered.  
#!/usr/bin/env python3

import boto3
import os
from datetime import datetime as dt
import time

client = boto3.client('ec2')

pipelineName = os.getenv('PIPELINE')
appValue = 'ubuntu-aws-tomcat-{}'.format(pipelineName)

filters = [
    {'Name':'tag:application', 'Values':[appValue]},
]

images = client.describe_images(Filters=filters)
if not images['Images']:
    print('Error: Image not found')
    exit(1)
else:
    print('Number of images found: {}'.format(str(len(images['Images']))))    
    for index, image in enumerate(images['Images']): #iterate through the AMIs that match the filter. 
        if index == 0:
            newestAMI = image #placeholder for the 'newest' AMI found so far
        else:
            # get creation dates for the newest AMI found so far, and the current AMI to compare with
            creationDateCurrentIteration = dt.strptime(image['CreationDate'], "%Y-%m-%dT%H:%M:%S.%fZ").timestamp()
            creationDatePrevNewest = dt.strptime(newestAMI['CreationDate'], "%Y-%m-%dT%H:%M:%S.%fZ").timestamp()

            if creationDatePrevNewest > creationDateCurrentIteration: # if the 'newest' AMI so far is newer than the current iteration AMI, deregister the current iteration AMI
                print('Deregistering: {} created on {}'.format(image['ImageId'],str(creationDateCurrentIteration)))
                client.deregister_image(
                    ImageId = image['ImageId']
                )
            else: # if the current iteration AMI is newer than the 'newest' AMI, deregister the 'newest' AMI and make the current iteration the new 'newest'
                print('Deregistering: {} created on {}'.format(newestAMI['ImageId'],str(creationDatePrevNewest)))
                client.deregister_image(
                    ImageId = newestAMI['ImageId']
                )
                newestAMI = image
