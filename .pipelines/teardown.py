#!/usr/bin/env python3

import boto3
import os

client = boto3.client('ec2')

pipelineName = os.getenv('PIPELINE')
appValue = 'ubuntu-aws-tomcat-{}'.format(pipelineName)

filters = [
    {'Name':'tag:application', 'Values':[appValue]},
]

images = client.describe_images(Filters=filters)
if not images['Images']:
    print('Error: Image not found')
    exit(1)
else:
    # Deregister all 'ecat' images marked as 'previous'
    for image in images['Images']:
        print('Deregistering: ' + image['ImageId'])
        client.deregister_image(
            ImageId = image['ImageId']
        )
