#!/bin/bash

# Goss is already installed in the base image, we just need to run it

# Replace existing tests
sudo cp -f /tmp/files/goss.yaml /opt/healthz

# Run tests
cd /opt/healthz
goss validate