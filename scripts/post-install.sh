#!/bin/bash

echo ==============================================================================
echo Configure Firewall 
echo ==============================================================================

## If you want to lock down HTTP
#sudo ufw allow from $HOME_IP_HTTP to any port 80

## If this box isn't going to be protected by a security group enable lock ip
#sudo ufw allow from $HOME_IP_SSH to any port 22

## If this box is only to be accessed by a jumpbox
#sudo ufw allow from 10.0.0.0/16 to any port 22

## Enable SSH access
sudo ufw allow ssh

## Enable HTTP
sudo ufw allow 8080

## Specifically disable tomcat shutdown port
sudo ufw deny 8005

## Turn it on
sudo ufw --force enable

echo ==============================================================================
echo Configure Tomcat #
echo ==============================================================================

# Remove default apps
sudo rm -rf /var/lib/tomcat8/webapps/*
# Remove version number from catalina
pushd /usr/share/tomcat8/lib
sudo jar xf catalina.jar org/apache/catalina/util/ServerInfo.properties
sudo sed -ie 's/server.info=Apache Tomcat.*$/server.info=Apache Tomcat/' \
org/apache/catalina/util/ServerInfo.properties
sudo jar uf catalina.jar org/apache/catalina/util/ServerInfo.properties
sudo rm -rf org
popd

# Create a default app
sudo mv /tmp/files/ROOT.war /var/lib/tomcat8/webapps/

# Set the error page
sudo sed -i 's/<\/web-app>//' /etc/tomcat8/web.xml
cat /tmp/files/web.xml | sudo tee --append /etc/tomcat8/web.xml
echo \n

# Configure logrotate
sudo mv -f /tmp/files/tomcat8 /etc/logrotate.d/tomcat8
sudo chown root:root /etc/logrotate.d/tomcat8
sudo chmod 644 /etc/logrotate.d/tomcat8 

# Make it run hourly
sudo mv /etc/cron.daily/logrotate /etc/cron.hourly/