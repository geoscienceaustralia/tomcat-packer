#!/bin/bash

sudo apt-get update

# Install tomcat
sudo apt-get install -y default-jdk
sudo apt-get install -y tomcat8

# Install firewall
sudo apt-get install -y ufw