
servicesToTest=snap.amazon-ssm-agent.amazon-ssm-agent,amazon-cloudwatch-agent,tomcat8,ufw
packagesToTest=python3
fileCheck=/etc/logrotate.d/tomcat8
allowPorts=22,8080
denyPorts=8005


python3 /tmp/image-testing/runtests.py --services $servicesToTest --packages $packagesToTest --files $fileCheck --allow $allowPorts --deny $denyPorts

exit $?